import { Component } from '@angular/core';
import {AppSideMenu, navQuickShortcut} from "./shared/app-lookups";
import {CashServiceService} from './services/util/cash-service.service';
import {UtilService} from './services/util/util.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  AppSideMenu: any[] = AppSideMenu;
  navQuickShortcut: any[] = navQuickShortcut;
  constructor(private cash: CashServiceService, private util: UtilService) {
  }

  // ----------- open screen -----------
  openSection(section) {}

  // ----------------- log out -------------
  logout = () => {}
}
