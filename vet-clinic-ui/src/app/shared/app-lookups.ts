// ------------------------- side menu items ---------------
export const AppSideMenu: any[] = [
  {title : 'home', icon: 'assets/img/icons/black/home.svg', route: '../home', iconPath: 'assets/img/icons/home.svg'},
  {title : 'pets', icon: 'assets/img/icons/black/pet.svg', route: '../pet', items: [
        {title: 'pets Screen', icon: 'accessibility_new', route: '../pet'},
        {title: 'new Pet', icon: 'accessibility_new', route: '../pet/form'}
    ], iconPath: 'assets/img/icons/pet.svg'
  },
  {
    title : 'owners', icon: 'assets/img/icons/black/user.svg', route: '../pet', items: [
      {title: 'owners screen', icon: 'accessibility_new', route: '../owner'},
      {title: 'new Owner', icon: 'accessibility_new', route: '../owner/form'},
    ], iconPath: 'assets/img/icons/user.svg'
  },
  {
    title : 'visits', icon: 'assets/img/icons/black/visit.svg', route: '../pet', items: [
      {title: 'visits screen', icon: 'accessibility_new', route: '../visit'},
      {title: 'new visit', icon: 'accessibility_new', route: '../visit/form'},
    ], iconPath: 'assets/img/icons/visit.svg'
  },
  {
    title : 'doctors', icon: 'assets/img/icons/black/doctor.svg', route: '../pet', items: [
      {title: 'doctors screen', icon: 'accessibility_new', route: '../doctor'},
      {title: 'new doctor', icon: 'accessibility_new', route: '../doctor/form'},
    ], iconPath: 'assets/img/icons/doctor.svg'
  },
  {
    title : 'clinics', icon: 'assets/img/icons/black/clinic.svg', route: '../pet', items: [
      {title: 'clinics screen', icon: 'accessibility_new', route: '../clinic'},
      {title: 'new clinic', icon: 'accessibility_new', route: '../clinic/form'},
    ], iconPath: 'assets/img/icons/clinic.svg'
  },
];


// ------------------------- nav bar Quick Shortcut list---------------
export const navQuickShortcut = [
  {title : 'home', icon: 'explore', route: '../home', iconPath: 'assets/img/icons/home.svg'},
  {title : 'pets', icon: 'explore', route: '../pet', iconPath: 'assets/img/icons/pet.svg'},
  {title : 'visits', icon: 'explore', route: '../visit', iconPath: 'assets/img/icons/visit.svg'},
  {title : 'doctors', icon: 'explore', route: '../doctor', iconPath: 'assets/img/icons/doctor.svg'},
];
export enum NOTIFICATION_TYPES {
  SUCCESS = 'success',
  INFO = 'info',
  WARNING = 'warning',
  ERROR = 'error'
}
export const KINDS_LOOKUP: any[] = [
  {name: 'DOG', value: 'DOG'},
  { name: 'CAT', value: 'CAT'}
];
export const GENDER_LOOKUP: any[] = [
  {name: 'MALE', value: 'MALE'},
  { name: 'FEMELLE', value: 'FEMELLE'}
];
export enum ACTIONS {
  SAVE = 'SAVE',
  UPDATE = 'UPDATE',
  DELETE = 'DELETE',
}
