const API_ENDPOINT = 'http://localhost:2200/vet/';
const APP_ORIGIN = window.location.origin;

export class APIUrls {
  public static pet = {
    url: API_ENDPOINT + 'pet/',
  };
  public static petOwner = {
    url: API_ENDPOINT + 'pet_owner/',
  };
  public static doctor = {
    url: API_ENDPOINT + 'doctor/',
  };
  public static clinic = {
    url: API_ENDPOINT + 'clinic/',
  };
  public static visit = {
    url: API_ENDPOINT + 'visit/',
  };
}
