export const  APP_CASH_KEY = 'VET_CLINIC';
export enum CASH_KEYS {
  USER = "USER"
}
export enum NAVIGATION {
  HOME = '../home',
  CLINIC_FORM = '../clinic/form',
  CLINIC = '../clinic',
  DOCTOR = '../doctor',
  DOCTOR_FORM = '../doctor/form',
  PET = '../pet',
  PET_FORM = '../pet/form',
  PET_OWNER = '../owner',
  PET_OWNER_FORM = '../owner/form',
  VISIT = '../visit',
  VISIT_FORM = '../visit/form',
}
export enum STATIC_ASSETS {
  empty = 'assets/img/empty_icon.JPG'
}
