"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var API_ENDPOINT = 'http://localhost:23000/mcm/';
var APIUrls = (function () {
    function APIUrls() {
    }
    APIUrls.Patient = { url: API_ENDPOINT + 'patient' };
    APIUrls.symptom = { url: API_ENDPOINT + 'symptom' };
    APIUrls.Article = { url: API_ENDPOINT + 'article' };
    APIUrls.Laboratoire = { url: API_ENDPOINT + 'labo' };
    APIUrls.appointment = {
        url: API_ENDPOINT + 'appointment',
        appointment_templates: API_ENDPOINT + 'appointment_template',
        today: API_ENDPOINT + 'appointment/search/todayAppointments',
    };
    APIUrls.medicament = { url: API_ENDPOINT + 'medicament' };
    APIUrls.payment = { url: API_ENDPOINT + 'payment' };
    APIUrls.checkup = {
        visit: { url: API_ENDPOINT + 'visit_checkup' },
        initial: { url: API_ENDPOINT + 'examination' }, url: API_ENDPOINT + 'checkup'
    };
    APIUrls.auth = { signUp: API_ENDPOINT + 'user/signUp', signIn: API_ENDPOINT + 'user/signIn' };
    APIUrls.waitingList = {
        checkIn: { add: API_ENDPOINT + 'check_in_patient' },
        url: API_ENDPOINT + 'waiting_list',
        sectionC: API_ENDPOINT + 'appointment/search/findByStartDate',
    };
    APIUrls.visit = {
        url: API_ENDPOINT + 'visit',
        cancel: API_ENDPOINT + 'update_visit',
        canceledVisits: API_ENDPOINT + 'visit/search/canceledVisits'
    };
    return APIUrls;
}());
exports.APIUrls = APIUrls;
