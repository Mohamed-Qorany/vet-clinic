import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DoctorComponent} from './doctor.component';
import {DoctorFormComponent} from './doctor-form/doctor-form.component';

const routes: Routes = [
  {path: '', component: DoctorComponent},
  {path: 'form', component: DoctorFormComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorRoutingModule { }
