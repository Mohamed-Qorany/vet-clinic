import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorRoutingModule } from './doctor-routing.module';
import { DoctorComponent } from './doctor.component';
import { DoctorFormComponent } from './doctor-form/doctor-form.component';
import {SharedModule} from '../common/shared.module';
import {FileManagerModule} from '../common/file-manager/file-manager.module';

@NgModule({
  declarations: [DoctorComponent, DoctorFormComponent],
  imports: [
    CommonModule,
    DoctorRoutingModule,
    SharedModule,
    FileManagerModule
  ]
})
export class DoctorModule { }
