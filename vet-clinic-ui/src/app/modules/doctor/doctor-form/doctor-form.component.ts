import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UtilService} from '../../../services/util/util.service';
import {ActivatedRoute} from '@angular/router';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';
import {APIUrls} from '../../../shared/api-urls';
import {NAVIGATION} from '../../../shared/app-config';
import {GENDER_LOOKUP, NOTIFICATION_TYPES} from '../../../shared/app-lookups';
import {UploadingBioComponent} from '../../common/file-manager/uploading-bio/uploading-bio.component';

@Component({
  selector: 'vet-doctor-form',
  templateUrl: './doctor-form.component.html',
  styleUrls: ['./doctor-form.component.css']
})
export class DoctorFormComponent implements OnInit {

  @ViewChild('uploadArea') uploadArea: UploadingBioComponent;
  formTitle;
  modelForm: FormGroup;
  selectedModel: any;
  loading: any;
  genders: any[] = GENDER_LOOKUP;
  clinics: any[] = [];
  bio: any;
  constructor(private fb: FormBuilder,
              private utilService: UtilService,
              private route: ActivatedRoute,
              private modalService: NzModalService,
              private notification: NzNotificationService) {
    this.route.queryParams.subscribe((params: any) => {
      this.formTitle = params['id'] ? 'Doctor Info' : 'Create Doctor';
      if (  params['id'] ) {
        this.getModelInfo( params['id'] );
      } else {
        this.selectedModel = null;
        this.createModelForm();
      }
    });
  }
  ngOnInit(): void {
    this.loadData();
    this.createModelForm();
  }

  // -------------- load Data -----------
  loadData() {
    this.loading = true;
    this.utilService.getRequest(APIUrls.clinic.url).subscribe(result => {
      this.clinics = result._embedded.clinic;
      this.loading = false;
    }, error => {});
  }


  // ------------ create model form  ----------
  createModelForm(selectedModel: any = {}) {
    this.modelForm = this.fb.group({
      bio: new FormControl(null, []),
      name: new FormControl(selectedModel.name, [Validators.required]),
      address: new FormControl(selectedModel.address, [Validators.required]),
      gender: new FormControl(selectedModel.gender, [Validators.required]),
      phone: new FormControl(selectedModel.phone, [Validators.required]),
      clinicId: new FormControl(selectedModel.clinicId, [Validators.required]),
    });
  }

  // ------------ get model info  ----------
  getModelInfo(id) {
    this.loading = true;
    this.utilService.getRequest(APIUrls.doctor.url + id).subscribe((result:any) => {
      if ( result.code === 1 && result.data) {
        this.selectedModel = result.data;
        this.createModelForm( this.selectedModel );
      } else {
        this.utilService.errorHandler('Get Doctor',
          'Something went Wrong While Get Doctor Data Please try Again Later!');
      }
      this.loading = false;
    }, error => this.utilService.errorHandler('Get Doctor',
      'Something went Wrong While Get Doctor Data Please try Again Later!'));
  }


  // ----------- create New -------------
  createNew = () => this.utilService.navigate(NAVIGATION.DOCTOR_FORM);

  // -------- delete Doctor --------------
  deleteModel() {
    this.modalService.confirm({
      nzTitle: 'Are you sure delete this Doctor?',
      nzContent: '<b style="color: red;">Once delete you will not able to restore</b>',
      nzOkText: 'Yes',
      nzOkType: 'danger',
      nzOnOk: () => {
        this.loading = true;
        this.utilService.deleteResource(APIUrls.doctor.url + this.selectedModel.id).subscribe((result: any) => {
          this.loading = false;
          if ( result.code === 1) {
            this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Delete Doctor', 'Doctor Deleted Successfully', {nzData: 1000});
            this.createNew();
          } else {
            this.utilService.errorHandler('Delete Doctor', 'Something went Wrong While Deleting Doctor Please try Again Later!');
          }
        }, error => this.utilService.errorHandler('Delete Doctor', 'Something went Wrong While Deleting Doctor Please try Again Later!'));
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel Delete Doctor')
    });
  }


  // --------------- submit Doctor form -----------------
  submitForm(): void {
    this.loading = true;
    this.modelForm.value.bio = this.bio;
    if ( !this.selectedModel ) {
      this.utilService.postRequest(APIUrls.doctor.url, this.modelForm.value).subscribe((result: any) => {
        this.loading = false;
        if ( result.code === 1 && result.data) {
          this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Create Doctor',
            'Doctor Created Successfully', {nzData: 1000});
          this.utilService.navigate(NAVIGATION.DOCTOR_FORM, {id: result.data.id});
        } else {
          this.utilService.errorHandler('Create Doctor', 'Something went Wrong While Create Doctor Please try Again Later!');
        }
      }, error => {
        this.utilService.errorHandler('Create Doctor', 'Something went Wrong While Create Doctor Please try Again Later!');
        this.loading = false;
      });
    } else {
      this.utilService.putResource(APIUrls.doctor.url + this.selectedModel.id, this.modelForm.value).subscribe((result: any) => {
        this.loading = false;
        if ( result.code === 1 && result.data) {
          this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Update Doctor',
            'Doctor Data Updated Successfully', {nzData: 1000});
        } else {
          this.utilService.errorHandler(
            'Update Doctor', 'Something went Wrong While Submit Doctor Data Please try Again Later!');
        }
      }, error => {
        this.utilService.errorHandler('Update Doctor', 'Something went Wrong While Submit Doctor Data Please try Again Later!');
        this.loading = false;
      });
    }
  }


  // ----------- bio select ----------
  handleFilesEvents(event) {
    if (event && event.action) {
      switch (event.action) {
        case 'FILE': {
          this.bio = event.file; break;
        }
      }
    }
  }
}
