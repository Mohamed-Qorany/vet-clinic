import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClinicRoutingModule } from './clinic-routing.module';
import { ClinicComponent } from './clinic.component';
import { ClinicFormComponent } from './clinic-form/clinic-form.component';
import {SharedModule} from '../common/shared.module';


@NgModule({
  declarations: [ClinicComponent, ClinicFormComponent],
  imports: [
    CommonModule,
    ClinicRoutingModule,
    SharedModule
  ]
})
export class ClinicModule { }
