import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UtilService} from '../../../services/util/util.service';
import {ActivatedRoute} from '@angular/router';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';
import {APIUrls} from '../../../shared/api-urls';
import {NAVIGATION} from '../../../shared/app-config';
import {NOTIFICATION_TYPES} from '../../../shared/app-lookups';

@Component({
  selector: 'vet-clinic-form',
  templateUrl: './clinic-form.component.html',
  styleUrls: ['./clinic-form.component.css']
})
export class ClinicFormComponent implements OnInit {

  formTitle;
  modelForm: FormGroup;
  selectedModel: any;
  loading: any;
  constructor(private fb: FormBuilder,
              private utilService: UtilService,
              private route: ActivatedRoute,
              private modalService: NzModalService,
              private notification: NzNotificationService) {
    this.route.queryParams.subscribe((params: any) => {
      this.formTitle = params['id'] ? 'Clinic Info' : 'Create Clinic';
      if (  params['id'] ) {
        this.getModelInfo( params['id'] );
      } else {
        this.selectedModel = null;
        this.createModelForm();
      }
    });
  }
  ngOnInit(): void {
    this.createModelForm();
  }

  // ------------ create model form  ----------
  createModelForm(selectedModel: any = {}) {
    this.modelForm = this.fb.group({
      name: new FormControl(selectedModel.name, [Validators.required]),
      address: new FormControl(selectedModel.address, [Validators.required]),
      email: new FormControl(selectedModel.email, [Validators.required, Validators.email]),
      phone: new FormControl(selectedModel.phone, [Validators.required]),
      workingDays: new FormControl(selectedModel.workingDays ? selectedModel.workingDays.split(',') : [], [Validators.required]),
      startTime: new FormControl(selectedModel.startTime, [Validators.required]),
      endTime: new FormControl(selectedModel.endTime, [Validators.required]),
    });
  }


  // ------------ get model info  ----------
  getModelInfo(id) {
    this.loading = true;
    this.utilService.getRequest(APIUrls.clinic.url + id).subscribe(result => {
      this.selectedModel = result;
      this.createModelForm( this.selectedModel );
      this.loading = false;
    }, error => this.utilService.errorHandler('Get Clinic',
      'Something went Wrong While Get Clinic Data Please try Again Later!'));
  }


  // ----------- create New -------------
  createNew = () => this.utilService.navigate(NAVIGATION.CLINIC_FORM);

  // -------- delete Clinic --------------
  deleteModel() {
    this.modalService.confirm({
      nzTitle: 'Are you sure delete this Clinic?',
      nzContent: '<b style="color: red;">Once delete you will not able to restore</b>',
      nzOkText: 'Yes',
      nzOkType: 'danger',
      nzOnOk: () => {
        this.loading = true;
        this.utilService.deleteResource(this.selectedModel._links.self.href).subscribe((value) => {
          this.loading = false;
          this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Delete Clinic', 'Clinic Deleted Successfully', {nzData: 1000});
          this.createNew();
        }, error => this.utilService.errorHandler('Delete Clinic', 'Something went Wrong While Deleting Clinic Please try Again Later!'));
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel Delete Clinic')
    });
  }


  // --------------- submit Clinic form -----------------
  submitForm(): void {
    this.loading = true;
    const body = {...this.modelForm.value};
    body.workingDays = body.workingDays.toString();
    if ( !this.selectedModel ) {
      this.utilService.postRequest(APIUrls.clinic.url, body).subscribe((result: any) => {
        this.loading = false;
        this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Create Clinic',
          'Clinic Created Successfully', {nzData: 1000});
        this.utilService.navigate(NAVIGATION.CLINIC_FORM, {id: result.id});
      }, error => {
        this.utilService.errorHandler('Create Clinic', 'Something went Wrong While Create Clinic Please try Again Later!');
        this.loading = false;
      });
    } else {
      this.utilService.putResource(this.selectedModel._links.self.href, body).subscribe(result => {
        this.loading = false;
        this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Update Clinic',
          'Clinic Data Updated Successfully', {nzData: 1000});
      }, error => {
        this.utilService.errorHandler('Update Clinic', 'Something went Wrong While Submit Clinic Data Please try Again Later!');
        this.loading = false;
      });
    }
  }

}
