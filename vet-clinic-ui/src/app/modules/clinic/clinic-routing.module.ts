import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ClinicComponent} from './clinic.component';
import {ClinicFormComponent} from './clinic-form/clinic-form.component';


const routes: Routes = [
  {path: '', component: ClinicComponent},
  {path: 'form', component: ClinicFormComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClinicRoutingModule { }
