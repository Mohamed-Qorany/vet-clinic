import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UtilService} from '../../../services/util/util.service';
import {ActivatedRoute} from '@angular/router';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';
import {APIUrls} from '../../../shared/api-urls';
import {NAVIGATION} from '../../../shared/app-config';
import {NOTIFICATION_TYPES} from '../../../shared/app-lookups';

@Component({
  selector: 'vet-visit-form',
  templateUrl: './visit-form.component.html',
  styleUrls: ['./visit-form.component.css']
})
export class VisitFormComponent implements OnInit {

  formTitle;
  modelForm: FormGroup;
  selectedModel: any;
  loading: any;
  pets: any[] = [];
  doctors: any[] = [];
  clinics: any[] = [];
  constructor(private fb: FormBuilder,
              private utilService: UtilService,
              private route: ActivatedRoute,
              private modalService: NzModalService,
              private notification: NzNotificationService) {
    this.route.queryParams.subscribe((params: any) => {
      this.formTitle = params['id'] ? 'Visit Info' : 'Create Visit';
      if (  params['id'] ) {
        this.getModelInfo( params['id'] );
      } else {
        this.selectedModel = null;
        this.createModelForm();
      }
    });
  }
  ngOnInit(): void {
    this.loadDoctors();
    this.loadClinics();
    this.loadPets();
  }

  // -------------- note that the following 3 methods can comes from one API (no time now) ------------
  // -------------- load Data -----------
  loadDoctors() {
    this.loading = true;
    this.utilService.getRequest(APIUrls.doctor.url).subscribe(result => {
      this.doctors = result._embedded.doctor;
      this.loading = false;
    }, error => {});
  }
  loadClinics() {
    this.loading = true;
    this.utilService.getRequest(APIUrls.clinic.url).subscribe(result => {
      this.clinics = result._embedded.clinic;
      this.loading = false;
    }, error => {});
  }
  loadPets() {
    this.loading = true;
    this.utilService.getRequest(APIUrls.pet.url).subscribe(result => {
      this.pets = result._embedded.pet;
      this.loading = false;
    }, error => {});
  }
  getClinic() {
    this.loading = true;
    this.utilService.getRequest(this.selectedModel._links.clinic.href).subscribe(result => {
      this.modelForm.controls['clinic'].setValue(result._links.self.href);
      this.loading = false;
    }, error => {});
  }
  getDoctor() {
    this.loading = true;
    this.utilService.getRequest(this.selectedModel._links.doctor.href).subscribe(result => {
      this.modelForm.controls['doctor'].setValue(result._links.self.href);
      this.loading = false;
    }, error => {});
  }

  // ------------ create model form  ----------
  createModelForm(selectedModel: any = {}) {
    const selectedPet = selectedModel && selectedModel._embedded && selectedModel._embedded.pet ?
      APIUrls.pet.url + selectedModel._embedded.pet.id : null;
    this.modelForm = this.fb.group({
      date: new FormControl(selectedModel.date, [Validators.required]),
      pet: new FormControl(selectedPet, [Validators.required]),
      doctor: new FormControl(selectedModel.doctor, [Validators.required]),
      clinic: new FormControl(selectedModel.clinic, [Validators.required]),
    });
  }

  // ------------ get model info  ----------
  getModelInfo(id) {
    this.loading = true;
    this.utilService.getRequest(APIUrls.visit.url + id).subscribe(result => {
      this.selectedModel = result;
      this.createModelForm( this.selectedModel );
      this.getClinic();
      this.getDoctor();
      this.loading = false;
    }, error => this.utilService.errorHandler('Get Visit',
      'Something went Wrong While Get Visit Data Please try Again Later!'));
  }


  // ----------- create New -------------
  createNew = () => this.utilService.navigate(NAVIGATION.VISIT_FORM);

  // -------- delete Visit --------------
  deleteModel() {
    this.modalService.confirm({
      nzTitle: 'Are you sure delete this Visit?',
      nzContent: '<b style="color: red;">Once delete you will not able to restore</b>',
      nzOkText: 'Yes',
      nzOkType: 'danger',
      nzOnOk: () => {
        this.loading = true;
        this.utilService.deleteResource(this.selectedModel._links.self.href).subscribe((value) => {
          this.loading = false;
          this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Delete Visit', 'Visit Deleted Successfully', {nzData: 1000});
          this.createNew();
        }, error => this.utilService.errorHandler('Delete Visit', 'Something went Wrong While Deleting Visit Please try Again Later!'));
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel Delete Visit')
    });
  }


  // --------------- submit Visit form -----------------
  submitForm(): void {
    this.loading = true;
    if ( !this.selectedModel ) {
      this.utilService.postRequest(APIUrls.visit.url, this.modelForm.value).subscribe((result: any) => {
        this.loading = false;
        this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Create Visit',
          'Visit Created Successfully', {nzData: 1000});
        this.utilService.navigate(NAVIGATION.VISIT_FORM, {id: result.id});
      }, error => this.utilService.errorHandler('Create Visit', 'Something went Wrong While Create Visit Please try Again Later!'));
    } else {
      this.utilService.putResource(this.selectedModel._links.self.href, this.modelForm.value).subscribe(result => {
        this.loading = false;
        this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Update Visit',
          'Visit Data Updated Successfully', {nzData: 1000});
      }, error => this.utilService.errorHandler('Update Visit', 'Something went Wrong While Submit Visit Data Please try Again Later!'));
    }
  }


}
