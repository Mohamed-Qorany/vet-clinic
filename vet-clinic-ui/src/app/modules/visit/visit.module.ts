import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VisitRoutingModule } from './visit-routing.module';
import { VisitComponent } from './visit.component';
import { VisitFormComponent } from './visit-form/visit-form.component';
import {SharedModule} from '../common/shared.module';


@NgModule({
  declarations: [VisitComponent, VisitFormComponent],
  imports: [
    CommonModule,
    VisitRoutingModule,
    SharedModule
  ]
})
export class VisitModule { }
