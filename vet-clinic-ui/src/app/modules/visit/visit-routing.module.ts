import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {VisitComponent} from './visit.component';
import {VisitFormComponent} from './visit-form/visit-form.component';


const routes: Routes = [
  {path: '', component: VisitComponent},
  {path: 'form', component: VisitFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisitRoutingModule { }
