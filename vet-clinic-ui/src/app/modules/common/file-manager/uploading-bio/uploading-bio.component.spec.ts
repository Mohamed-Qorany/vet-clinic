import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadingBioComponent } from './uploading-bio.component';

describe('UploadingBioComponent', () => {
  let component: UploadingBioComponent;
  let fixture: ComponentFixture<UploadingBioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadingBioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadingBioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
