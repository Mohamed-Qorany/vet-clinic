import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FileUploader} from 'ng2-file-upload';
import {FileManagerService} from '../../../../services/util/file-manager.service';

@Component({
  selector: 'vet-uploading-bio',
  templateUrl: './uploading-bio.component.html',
  styleUrls: ['./uploading-bio.component.css']
})
export class UploadingBioComponent implements OnInit {

  @Output() fileEvent: EventEmitter<any> =  new EventEmitter();
  @Input() fileDataUrl;
  file: any = {};
  public uploader: FileUploader = new FileUploader({url: '', disableMultipart: true});

  constructor(private _FileManagerService: FileManagerService) { }

  ngOnInit(): void {
  }


  // ------------ on file selected ------------------
  onFileSelected(event: any) {
    const ref = this;
    if ( event && event.length > 0) {
      const reader = new FileReader();
      for ( let i = 0; i <  event.length; i++) {
        const file: File = event[i];
        this._FileManagerService.readFileBytes(file).then(function(data) {
          ref.file = {fileName: file.name, fileSize: file.size, fileType: file.type, content: data};
          ref.fileEvent.emit({action: 'FILE', file: ref.file});
        });
        reader.onload = (event: any) => ref.fileDataUrl = event.target.result;
        reader.readAsDataURL(file);
      }
    }
  }
}
