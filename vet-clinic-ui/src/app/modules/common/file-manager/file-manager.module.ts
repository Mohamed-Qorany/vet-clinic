import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadingAreaComponent } from './uploading-area/uploading-area.component';
import {FileUploadModule} from 'ng2-file-upload';
import {SharedModule} from '../shared.module';
import { UploadingBioComponent } from './uploading-bio/uploading-bio.component';

@NgModule({
  declarations: [UploadingAreaComponent, UploadingBioComponent],
  imports: [
    CommonModule, FileUploadModule, SharedModule
  ],
  exports: [UploadingAreaComponent, UploadingBioComponent]
})
export class FileManagerModule { }
