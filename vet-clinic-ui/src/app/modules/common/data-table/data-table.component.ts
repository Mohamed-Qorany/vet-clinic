import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {UtilService} from '../../../services/util/util.service';


@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataTableComponent implements OnInit, OnChanges {

  @Input() title: any;
  @Input() footer: any;
  @Input() indexed;
  @Input() dateFields: any[] = [];
  @Input() listOfData: any[] = [];
  @Input() listOfColumns: any[] = [];
  @Input() listOfColumnsTitles: any[] = [];
  @Input() listOfActions: any[] = [];
  @Input() buttonsActons: boolean;
  @Input() displayData: any[] =[];
  @Input() loading: boolean;
  @Input() pagination = true;
  @Input() expandable;
  @Input() checkbox;
  @Input() allChecked;
  @Input() indeterminate = false;
  @Output() datatableEvent = new EventEmitter<any>();
  sortName: string | null = null;
  sortValue: string | null = null;
  originalListOfData: any[];

  constructor( private  util: UtilService) {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.displayData = this.listOfData;
    if ( this.listOfData ) {
      this.originalListOfData = Object.assign([], this.listOfData);
    }
  }


  currentPageDataChange($event: any[]): void {
    this.displayData = $event;
    // this.refreshStatus();
  }

  refreshStatus(): void {
    const validData = this.displayData.filter(value => !value.disabled);
    const allChecked = validData.length > 0 && validData.every(value => value.checked === true);
    const allUnChecked = validData.every(value => !value.checked);
    this.allChecked = allChecked;
    this.indeterminate = !allChecked && !allUnChecked;
  }

  // -------------- select all columns ----------------
  checkAll(value: boolean): void {
    this.displayData.forEach(data => {
      if (!data.disabled) {
        data.checked = value;
      }
    });
    this.refreshStatus();
  }

  // -------------- emit row action ----------------
  emitRowAction = (actionName, row, index) => this.datatableEvent.emit({name: actionName, rowData: row, rowIndex: index});

  // -------------- sort column  ----------------
  sort(sort: { key: string; value: string }): void {
    this.sortName = sort.key;
    this.sortValue = sort.value;
    this.search();
  }

  search(): void {
    if (this.sortName && this.sortValue && this.originalListOfData) {
      const allData = Object.assign([], this.originalListOfData);
      if ( this.dateFields && this.dateFields.indexOf(this.sortName) !== -1) {
        // this.listOfData = allData.sort((a, b) =>);
      } else {
        this.listOfData = allData.sort((a, b) =>
          this.sortValue === 'ascend' ? ( a[this.sortName!] > b[this.sortName!] ? 1 : -1 )
            : ( b[this.sortName!] > a[this.sortName!] ? 1 : -1)
        );
      }
    } else {
      this.listOfData = this.originalListOfData;
    }
  }
}
