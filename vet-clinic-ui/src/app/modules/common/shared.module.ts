import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {DataTableModule} from './data-table/data-table.module';
import {LoadingBarModule} from './loading-bar/loading-bar.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgZorroAntdModule,
    DataTableModule,
    LoadingBarModule
  ],
  exports: [
   FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    DataTableModule,
    LoadingBarModule
  ],
  declarations: []
})
export class SharedModule { }
