import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoadingBarComponent} from './loading-bar.component';
import {NgZorroAntdModule} from 'ng-zorro-antd';



@NgModule({
  declarations: [LoadingBarComponent],
  imports: [
    CommonModule,
    NgZorroAntdModule
  ],
  exports: [LoadingBarComponent]
})
export class LoadingBarModule { }
