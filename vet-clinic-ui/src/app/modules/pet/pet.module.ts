import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PetRoutingModule } from './pet-routing.module';
import { PetComponent } from './pet.component';
import { PetFormComponent } from './pet-form/pet-form.component';
import {SharedModule} from '../common/shared.module';


@NgModule({
  declarations: [PetComponent, PetFormComponent],
  imports: [
    CommonModule,
    PetRoutingModule,
    SharedModule
  ]
})
export class PetModule { }
