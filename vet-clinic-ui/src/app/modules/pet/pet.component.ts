import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {UtilService} from '../../services/util/util.service';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';
import {APIUrls} from '../../shared/api-urls';
import {NAVIGATION} from '../../shared/app-config';
import {NOTIFICATION_TYPES} from '../../shared/app-lookups';

@Component({
  selector: 'vet-pet',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.css']
})
export class PetComponent implements OnInit {

  loading;
  searchCtrl = new FormControl();
  listOfData: any[] = [];
  listOfDataFiltered: any[] = [];
  listOfColumns: any[] = ['name', 'gender', 'kind', 'weight', 'owner_id'];
  listOfColumnsTitles: any[] = ['name', 'gender', 'kind', 'weight', 'owner_id'];
  listOfActions: any[] = [
    {name: 'Edit', icon: 'edit'},
    {name: 'Delete', icon: 'delete'},
  ];
  constructor(private  utilService: UtilService,
              private notification: NzNotificationService,
              private modalService: NzModalService) {
    this.searchCtrl.valueChanges.subscribe(value => {
      if ( !value ) { this.loadData(); }
    });
  }
  ngOnInit() {
    this.loadData();
  }


  // -------------- search -----------
  search() {}

  // -------------- load Data -----------
  loadData() {
    this.loading = true;
    this.utilService.getRequest(APIUrls.pet.url).subscribe(result => {
      this.listOfData = result._embedded.pet;
      this.listOfDataFiltered = result._embedded.pet;
      this.loading = false;
    }, error => {});
  }

  // -------------- datatable Event Handle -----------
  datatableEventHandle(event) {
    switch (event.name) {
      case 'Edit': this.utilService.navigate(NAVIGATION.PET_FORM, {id: event.rowData.id}); break;
      case 'Delete': this.delete(event.rowData, event.rowIndex); break;
    }
  }

  // ---------------- -delete -------------
  delete(pet, index) {
    this.modalService.confirm({
      nzTitle: 'Are you sure delete this Pet?',
      nzContent: '<b style="color: red;">Once delete you will not able to restore</b>',
      nzOkText: 'Yes',
      nzOkType: 'danger',
      nzOnOk: () => {
        this.loading = true;
        this.utilService.deleteResource(pet._links.self.href).subscribe((result) => {
          this.listOfData = this.listOfData.filter((value, valueIndex)  => valueIndex !== index);
          this.listOfDataFiltered = this.listOfData;
          this.loading = false;
          this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Delete Pet',
            'Pet Deleted Successfully', {nzData: 1000});
        }, error => {});
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel Delete Pet')
    });
  }

}
