import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PetOwnerComponent} from './pet-owner.component';
import {OwnerFormComponent} from './owner-form/owner-form.component';


const routes: Routes = [
  {path: '', component: PetOwnerComponent},
  {path: 'form', component: OwnerFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PetOwnerRoutingModule { }
