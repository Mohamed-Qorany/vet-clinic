import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UtilService} from '../../../../services/util/util.service';
import {ActivatedRoute} from '@angular/router';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';
import {APIUrls} from '../../../../shared/api-urls';
import {NAVIGATION} from '../../../../shared/app-config';
import {GENDER_LOOKUP, NOTIFICATION_TYPES} from '../../../../shared/app-lookups';

@Component({
  selector: 'vet-owner-form',
  templateUrl: './owner-form.component.html',
  styleUrls: ['./owner-form.component.css']
})
export class OwnerFormComponent implements OnInit {

  formTitle;
  modelForm: FormGroup;
  selectedModel: any;
  loading: any;
  genders: any[] = GENDER_LOOKUP;
  constructor(private fb: FormBuilder,
              private utilService: UtilService,
              private route: ActivatedRoute,
              private modalService: NzModalService,
              private notification: NzNotificationService) {
    this.route.queryParams.subscribe((params: any) => {
      this.formTitle = params['id'] ? 'Owner Info' : 'Create Owner';
      if (  params['id'] ) {
        this.getModelInfo( params['id'] );
      } else {
        this.selectedModel = null;
        this.createModelForm();
      }
    });
  }
  ngOnInit(): void {
    this.createModelForm();
  }

  // ------------ create model form  ----------
  createModelForm(selectedModel: any = {}) {
    this.modelForm = this.fb.group({
      name: new FormControl(selectedModel.name, [Validators.required]),
      email: new FormControl(selectedModel.email, [Validators.required, Validators.email]),
      phone: new FormControl(selectedModel.phone, [Validators.required]),
      gender: new FormControl(selectedModel.gender, [Validators.required]),
      address: new FormControl(selectedModel.address, [Validators.required]),
    });
  }


  // ------------ get model info  ----------
  getModelInfo(id) {
    this.loading = true;
    this.utilService.getRequest(APIUrls.petOwner.url + id).subscribe(result => {
      this.selectedModel = result;
      this.createModelForm( this.selectedModel );
      this.loading = false;
    }, error => this.utilService.errorHandler('Get Owner',
      'Something went Wrong While Get Owner Data Please try Again Later!'));
  }


  // ----------- create New -------------
  createNew = () => this.utilService.navigate(NAVIGATION.PET_OWNER_FORM);

  // -------- delete Owner --------------
  deleteModel() {
    this.modalService.confirm({
      nzTitle: 'Are you sure delete this Owner?',
      nzContent: '<b style="color: red;">Once delete you will not able to restore</b>',
      nzOkText: 'Yes',
      nzOkType: 'danger',
      nzOnOk: () => {
        this.loading = true;
        this.utilService.deleteResource(this.selectedModel._links.self.href).subscribe((value) => {
          this.loading = false;
          this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Delete Owner', 'Owner Deleted Successfully', {nzData: 1000});
          this.createNew();
        }, error => this.utilService.errorHandler('Delete Owner', 'Something went Wrong While Deleting Owner Please try Again Later!'));
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel Delete Owner')
    });
  }


  // --------------- submit Owner form -----------------
  submitForm(): void {
    this.loading = true;
    if ( !this.selectedModel ) {
      this.utilService.postRequest(APIUrls.petOwner.url, this.modelForm.value).subscribe((result: any) => {
        this.loading = false;
        this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Create Owner',
          'Owner Created Successfully', {nzData: 1000});
        this.utilService.navigate(NAVIGATION.PET_OWNER_FORM, {id: result.id});
      }, error => {
        this.utilService.errorHandler('Create Owner', 'Something went Wrong While Create Owner Please try Again Later!');
        this.loading = false;
      });
    } else {
      this.utilService.putResource(this.selectedModel._links.self.href, this.modelForm.value).subscribe(result => {
        this.loading = false;
        this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Update Owner',
          'Owner Data Updated Successfully', {nzData: 1000});
      }, error => {
        this.utilService.errorHandler('Update Owner', 'Something went Wrong While Submit Owner Data Please try Again Later!');
        this.loading = false;
      });
    }
  }

}
