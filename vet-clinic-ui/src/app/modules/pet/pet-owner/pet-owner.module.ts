import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PetOwnerRoutingModule } from './pet-owner-routing.module';
import { PetOwnerComponent } from './pet-owner.component';
import { OwnerFormComponent } from './owner-form/owner-form.component';
import {SharedModule} from '../../common/shared.module';


@NgModule({
  declarations: [PetOwnerComponent, OwnerFormComponent],
  imports: [
    CommonModule,
    PetOwnerRoutingModule,
    SharedModule
  ]
})
export class PetOwnerModule { }
