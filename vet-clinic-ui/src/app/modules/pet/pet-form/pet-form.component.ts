import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UtilService} from '../../../services/util/util.service';
import {ActivatedRoute} from '@angular/router';
import {NzModalService, NzNotificationService} from 'ng-zorro-antd';
import {APIUrls} from '../../../shared/api-urls';
import {NAVIGATION} from '../../../shared/app-config';
import {GENDER_LOOKUP, KINDS_LOOKUP, NOTIFICATION_TYPES} from '../../../shared/app-lookups';

@Component({
  selector: 'vet-pet-form',
  templateUrl: './pet-form.component.html',
  styleUrls: ['./pet-form.component.css']
})
export class PetFormComponent implements OnInit {

  formTitle;
  modelForm: FormGroup;
  selectedModel: any;
  loading: any;
  kinds: any[] = KINDS_LOOKUP;
  genders: any[] = GENDER_LOOKUP;
  owners: any[] = [];
  constructor(private fb: FormBuilder,
              private utilService: UtilService,
              private route: ActivatedRoute,
              private modalService: NzModalService,
              private notification: NzNotificationService) {
    this.route.queryParams.subscribe((params: any) => {
      this.formTitle = params['id'] ? 'Pet Info' : 'Create pet';
      if (  params['id'] ) {
        this.getModelInfo( params['id'] );
      } else {
        this.selectedModel = null;
        this.createModelForm();
      }
    });
  }
  ngOnInit(): void {
    this.loadData();
    this.createModelForm();
  }

  // -------------- load Data -----------
  loadData() {
    this.loading = true;
    this.utilService.getRequest(APIUrls.petOwner.url).subscribe(result => {
      this.owners = result._embedded.pet_owner;
      this.loading = false;
    }, error => {});
  }

  // ------------ create model form  ----------
  createModelForm(selectedModel: any = {}) {
    this.modelForm = this.fb.group({
      name: new FormControl(selectedModel.name, [Validators.required]),
      kind: new FormControl(selectedModel.kind, [Validators.required]),
      weight: new FormControl(selectedModel.weight, [Validators.required]),
      gender: new FormControl(selectedModel.gender, [Validators.required]),
      dob: new FormControl(selectedModel.dob, [Validators.required]),
      owner: new FormControl(selectedModel.owner, [Validators.required]),
    });
  }

  // ------------ get model info  ----------
  getModelInfo(id) {
    this.loading = true;
    this.utilService.getRequest(APIUrls.pet.url + id).subscribe(result => {
      this.selectedModel = result;
      this.getOwnerInfo(this.selectedModel._links.owner.href);
    }, error => this.utilService.errorHandler('Get Pet',
      'Something went Wrong While Get Pet Data Please try Again Later!'));
  }
  getOwnerInfo(url) {
    this.loading = true;
    this.utilService.getRequest(url).subscribe(result => {
      this.selectedModel.owner = result._links.self.href;
      this.createModelForm( this.selectedModel );
      this.loading = false;
    }, error => this.utilService.errorHandler('Get Pet',
      'Something went Wrong While Get Pet Data Please try Again Later!'));
  }


  // ----------- create New -------------
  createNew = () => this.utilService.navigate(NAVIGATION.PET_FORM);

  // -------- delete Pet --------------
  deleteModel() {
    this.modalService.confirm({
      nzTitle: 'Are you sure delete this Pet?',
      nzContent: '<b style="color: red;">Once delete you will not able to restore</b>',
      nzOkText: 'Yes',
      nzOkType: 'danger',
      nzOnOk: () => {
        this.loading = true;
        this.utilService.deleteResource(this.selectedModel._links.self.href).subscribe((value) => {
          this.loading = false;
          this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Delete Pet', 'Pet Deleted Successfully', {nzData: 1000});
          this.createNew();
        }, error => this.utilService.errorHandler('Delete Pet', 'Something went Wrong While Deleting Pet Please try Again Later!'));
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel Delete Pet')
    });
  }


  // --------------- submit Pet form -----------------
  submitForm(): void {
    this.loading = true;
    if ( !this.selectedModel ) {
      this.utilService.postRequest(APIUrls.pet.url, this.modelForm.value).subscribe((result: any) => {
        this.loading = false;
        this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Create Pet',
          'Pet Created Successfully', {nzData: 1000});
        this.utilService.navigate(NAVIGATION.PET_FORM, {id: result.id});
      }, error => {
        this.utilService.errorHandler('Create Pet', 'Something went Wrong While Create Pet Please try Again Later!');
        this.loading = false;
      });
    } else {
      this.utilService.putResource(this.selectedModel._links.self.href, this.modelForm.value).subscribe(result => {
        this.loading = false;
        this.notification.create(NOTIFICATION_TYPES.SUCCESS, 'Update Pet',
          'Pet Data Updated Successfully', {nzData: 1000});
      }, error => {
        this.utilService.errorHandler('Update Pet', 'Something went Wrong While Submit Pet Data Please try Again Later!');
        this.loading = false;
      });
    }
  }

}
