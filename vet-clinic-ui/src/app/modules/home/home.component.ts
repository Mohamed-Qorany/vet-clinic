import { Component, OnInit } from '@angular/core';
import {UtilService} from '../../services/util/util.service';
import {AppSideMenu} from '../../shared/app-lookups';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  dashBoardCards: any[] = AppSideMenu;

  constructor(private util: UtilService) {}
  ngOnInit(): void {}

  // ----------- open screen -----------
  openSection = (section) => this.util.navigate(section.route);
}
