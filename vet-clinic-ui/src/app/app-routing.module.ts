import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: 'home', loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)},
  { path: 'pet', loadChildren: () => import('./modules/pet/pet.module').then(m => m.PetModule)},
  { path: 'owner', loadChildren: () => import('./modules/pet/pet-owner/pet-owner.module').then(m => m.PetOwnerModule)},
  { path: 'doctor', loadChildren: () => import('./modules/doctor/doctor.module').then(m => m.DoctorModule)},
  { path: 'clinic', loadChildren: () => import('./modules/clinic/clinic.module').then(m => m.ClinicModule)},
  { path: 'visit', loadChildren: () => import('./modules/visit/visit.module').then(m => m.VisitModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
