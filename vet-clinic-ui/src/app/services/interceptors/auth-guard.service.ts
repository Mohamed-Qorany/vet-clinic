import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {CashServiceService} from '../util/cash-service.service';
import {CASH_KEYS, NAVIGATION} from '../../shared/app-config';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {


   constructor(private cash: CashServiceService, private router: Router) {}

   canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      const notInSession = !this.cash.getSessionData(CASH_KEYS.USER) || !this.cash.getSessionData(CASH_KEYS.USER).userToken;
      let notInLocal;
      if ( notInSession ) {
         notInLocal = !this.cash.getLocalStorage(CASH_KEYS.USER) || !this.cash.getLocalStorage(CASH_KEYS.USER).userToken;
         if ( !notInLocal ) {
           this.cash.setSessionData(CASH_KEYS.USER, this.cash.getLocalStorage(CASH_KEYS.USER));
         }
      }
      if ( notInSession && notInLocal) {
        this.router.navigate([NAVIGATION.AUTH]);
        return true;
      } else {
        return true;
      }
    }
}
