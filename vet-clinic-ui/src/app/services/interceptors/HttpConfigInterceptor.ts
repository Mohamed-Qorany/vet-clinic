import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Injectable} from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import {CashServiceService} from '../util/cash-service.service';
import {CASH_KEYS} from '../../shared/app-config';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

  constructor(private cash: CashServiceService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = this.cash.getSessionData(CASH_KEYS.USER) ? this.cash.getSessionData(CASH_KEYS.USER).userToken : null;
    if (token) {
      request = request.clone({ headers: request.headers.set('token', token) });
    }
    if (!request.headers.has('Content-Type')) {
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }
    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }), catchError((error: HttpErrorResponse) => {
        let data = {};
        data = { reason: error && error.error.reason ? error.error.reason : '',  status: error.status };
        return throwError(error);
      })
    );
  }
}
export class HttpConfiFilterPipegInterceptor {}
