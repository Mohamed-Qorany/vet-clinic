import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {NOTIFICATION_TYPES} from '../../shared/app-lookups';
import {NzNotificationService} from 'ng-zorro-antd';

@Injectable({providedIn: 'root'})
export class UtilService {

    constructor(private http: HttpClient, private notification: NzNotificationService, private router: Router) {}

    // --------------- http ---------------
    getRequest(path, params: any = {}) {return this.http.get<any>(path, { params: params }); }
    postRequest(path, body) {return this.http.post(path, body); }
    putResource(path, body) {return this.http.put(path, body); }
    deleteResource(path) {return this.http.delete(path); }

    // --------------- navigate ---------------
    navigate(path, params = {}) { this.router.navigate([path],{ queryParams: params}); }

    // ----------- error handle -----------
    errorHandler = (errorTitle, errorMsg) => this.notification.create(NOTIFICATION_TYPES.ERROR, errorTitle, errorMsg, {nzData: 1000});
}
