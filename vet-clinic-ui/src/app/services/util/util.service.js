"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core/core");
require("rxjs/add/operator/map");
var UtilService = (function () {
    function UtilService(http, router) {
        this.http = http;
        this.router = router;
    }
    /* getRequest(path) {return this.http.get(path).map(response => response.json()); }
     postRequest(path, body) {return this.http.post(path, body).map((response: Response) => response.json()); }
     putRequest(path, body) {return this.http.put(path, body).map((response: Response) => response.json()); }
     deleteRequest(path) {return this.http.delete(path).map((response: Response) => response.json()); }*/
    UtilService.prototype.getRequest = function (path) { return this.http.get(path); };
    UtilService.prototype.postRequest2 = function (path, body) { return this.http.post(path, body); };
    UtilService.prototype.postRequest = function (path, body) { return this.http.post(path, body); };
    UtilService.prototype.postResource = function (path, body) { return this.http.post(path, body); };
    UtilService.prototype.putRequest = function (path, body) { return this.http.put(path, body).map(function (response) { return response.json(); }); };
    UtilService.prototype.putResource = function (path, body) { return this.http.put(path, body); };
    UtilService.prototype.deleteRequest = function (path) { return this.http.delete(path).map(function (response) { return response.json(); }); };
    UtilService.prototype.deleteResource = function (path) { return this.http.delete(path); };
    UtilService.prototype.navigate = function (path) { this.router.navigate([path]); };
    UtilService = __decorate([
        core_1.Injectable()
    ], UtilService);
    return UtilService;
}());
exports.UtilService = UtilService;
