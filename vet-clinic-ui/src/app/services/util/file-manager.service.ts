import { Injectable } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
declare var $: any;
import * as FileSaver from 'file-saver';


@Injectable({
  providedIn: 'root'
})
export class FileManagerService {

  constructor() { }


  // ------------- capture html content to pdf ----------------
  printCanvas(dataUrl) {
    let windowContent = '<!DOCTYPE html>';
    windowContent += '<html>';
    windowContent += '<head><title></title></head>';
    windowContent += '<body>';
    windowContent += '<img src="' + dataUrl + '">';
    windowContent += '</body>';
    windowContent += '</html>';
    let popupWin;
    popupWin = window.open('', '_blank', 'top=0,left=20,scrollbars=yes,resizable=yes,height=' + screen.availHeight  + ',width=' + (screen.availWidth - 40));
    popupWin.document.open();
    popupWin.document.text_title = '';
    popupWin.document.write(windowContent);
    popupWin.document.close();
    let printFinished;
    let id;
    const printIframeInterval = () => {
      if ( popupWin.document.body.innerHTML) {
        popupWin.focus();
        popupWin.print();
        printFinished = true;
      }
      if ( printFinished) {
        clearInterval(id);
      }
    }
    id = setInterval(printIframeInterval, 10);
  }


  // ------------- capture html content to pdf ----------------
  print(containerId, fileName, componentRef, width=208, hideInPrint: string[] = [], printCanvas: boolean = false){
    let ref = this;
    ref.preparePrintContent(hideInPrint);
    componentRef.loading = true;
    const data = document.getElementById(containerId);
      html2canvas(data).then(canvas => {
        // Few necessary setting options
        var imgWidth = width;
        var pageHeight = 295;
        var imgHeight = canvas.height * imgWidth / canvas.width;
        var heightLeft = imgHeight;
        const contentDataURL = canvas.toDataURL('image/png');
        if ( !printCanvas ){
          let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
          var position = 5;
          pdf.addImage(contentDataURL, 'PNG', 5, position, imgWidth, imgHeight);
          pdf.save(fileName); // Generated PDF

        } else{
          this.printCanvas(canvas.toDataURL());
        }
        ref.preparePrintContent(hideInPrint);
        componentRef.loading = false;
      });
  }

  // ------------- prepare Print Content before print -----------
  preparePrintContent(hideInPrint: string[] = []){
    if ( hideInPrint && hideInPrint.length > 0) {
      hideInPrint.forEach( element => {
        $(element).toggleClass('printOnly');
        $('.notPrintable').toggleClass('printOnly');
      });
    }
  }

  // ------------- read file as byte[] -----------
  readFileBytes(file): Promise<any> {
    const reader  = new FileReader();
    const future = new Promise((resolve, reject) => {
      reader.addEventListener("load", function () {
        const arrayBuffer:any = reader.result;
        const array = new Uint8Array(arrayBuffer);
        const fileByteArray: any [] = [];
        for (let i = 0; i < array.length; i++) {
          fileByteArray.push(array[i]);
        }
        resolve( fileByteArray);
      }, false);
      reader.addEventListener("error", function (event) {
        reject(event);
      }, false);
      reader.readAsArrayBuffer(file);
    });
    return future;
  }

 // ------------- read file as base 64 -----------
 readFileBase64(file): Promise<any> {
     const reader  = new FileReader();
     const future = new Promise((resolve, reject) => {
      reader.addEventListener("load", function () {
        resolve(reader.result);
      }, false);
      reader.addEventListener("error", function (event) {
        reject(event);
      }, false);
      reader.readAsDataURL(file);
    });
    return future;
  }

  // -------------- download file from byte array --------------
  downloadFileByteArray(content, type, fileName) {
    const file = new Blob([this._base64ToArrayBuffer(content)], {type: type});
    FileSaver.saveAs(file, fileName);
  }

  // ------------- download file  -----------
  downloadFile(content, type, fileName, filePath = null){
    const file = new Blob([filePath ? this._base64ToArrayBuffer(content) : this.byteArrayToUint8Array(content)], {type: type});
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(file);
    } else {
      var objectUrl = URL.createObjectURL(file);
      window.open(objectUrl);
    }
    // FileSaver.saveAs(content, fileName);
  }

  // ------------- byte Array To Uint8 Array  -----------
  byteArrayToUint8Array(byteArray): any {
    let uint8Array = new Uint8Array(byteArray.length);
    for(let i = 0; i < byteArray.length; i++) {
      uint8Array[i] = byteArray[i];
    }
    return uint8Array;
  }

  _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes;
  }
}
