import { Injectable } from '@angular/core';
import {APP_CASH_KEY} from '../../shared/app-config';


@Injectable({providedIn: 'root'})
export class CashServiceService {

    constructor() {}

    saveLocalStorage(key, value) { localStorage.setItem(APP_CASH_KEY + key, JSON.stringify(value)); }
    removeLocalStorage(key) { localStorage.removeItem(APP_CASH_KEY + key); }
    getLocalStorage(key): any {return JSON.parse(localStorage.getItem(APP_CASH_KEY + key)); }

    setSessionData(key, data) { sessionStorage.setItem(APP_CASH_KEY + key, JSON.stringify(data)); }
    getSessionData(key): any { return JSON.parse(sessionStorage.getItem(APP_CASH_KEY + key)); }
    deleteSessionData(key) { sessionStorage.removeItem(APP_CASH_KEY + key); }
    clearSession() { sessionStorage.clear(); }
}
