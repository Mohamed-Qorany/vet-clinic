package com.vet.clinic.service;

import com.vet.clinic.entity.Doctor;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler(Doctor.class)
public class DoctorEventHandler {
	
	@HandleBeforeCreate
    public void handleAfterCreateOrSave(Doctor doctor) {

    }
}
