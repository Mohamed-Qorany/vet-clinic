package com.vet.clinic.service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.vet.clinic.model.FileModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

@Service
public class AmazonClient {

    private AmazonS3 s3client;

    @Value("${endpointUrl}")
    private String endpointUrl;
    @Value("${bucketName}")
    private String bucketName;
    @Value("${accessKey}")
    private String accessKey;
    @Value("${secretKey}")
    private String secretKey;

    @PostConstruct
    private void initializeAmazon() {
        AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
        this.s3client = new AmazonS3Client(credentials);
    }

    // ------------------ upload file ----------
    public String uploadFile(FileModel fileModel) {
        String fileUrl = "";
        try {
            fileUrl = endpointUrl + "/" + bucketName + "/" + fileModel.getFileName();
            if ( !isFileExist(fileUrl)){
                File file = new File(fileModel.getFileName());
                OutputStream os = new FileOutputStream(file);
                os.write(fileModel.getContent());
                s3client.putObject(new PutObjectRequest(bucketName, fileModel.getFileName(), file)
                        .withCannedAcl(CannedAccessControlList.PublicRead));
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileUrl;
    }

    // ------------------ delete file ----------
    public void deleteFile(String fileUrl) {
        String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
        s3client.deleteObject(new DeleteObjectRequest(bucketName + "/", fileName));
    }

    // ------------------ delete file ----------
    public boolean isFileExist(String fileUrl) {
        String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
        return s3client.doesObjectExist(bucketName, fileName);
    }
}