package com.vet.clinic;

import com.vet.clinic.enums.ResponseCodes;
import com.vet.clinic.model.VetResponse;
import org.springframework.http.ResponseEntity;

public class VetUtil {

    private VetUtil() { }

    public static ResponseEntity buildResponse(ResponseCodes responseCode, Object data){
        return ResponseEntity.ok(new VetResponse(responseCode.getCode(),
                responseCode.getDescription(), data));
    }
}
