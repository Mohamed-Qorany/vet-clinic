package com.vet.clinic.entity.projection;

import com.vet.clinic.entity.Pet;
import com.vet.clinic.entity.PetOwner;
import com.vet.clinic.enums.GenderEnum;
import com.vet.clinic.enums.PetKindEnum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;

@Projection(name = "petList", types = { Pet.class })
public interface PetProjection {

	@Value("#{target.id}")
	long getId();

	String getName();
	Date getDob();
	long getWeight();
	GenderEnum getGender();
	PetKindEnum getKind();
	PetOwner getOwner();
}