package com.vet.clinic.entity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vet.clinic.entity.base.EntityBaseUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PetOwner extends EntityBaseUser implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(unique = true, nullable = false)
	private String email;
	@Column(nullable = false)
	private String address;

	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Pet> pets;
}
