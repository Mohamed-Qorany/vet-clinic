package com.vet.clinic.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vet.clinic.entity.base.EntityBaseId;
import com.vet.clinic.enums.GenderEnum;
import com.vet.clinic.enums.PetKindEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Pet extends EntityBaseId implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private Date dob;
	@Column(nullable = false)
	private long weight;
	@Column(nullable = false)
	private GenderEnum gender;
	@Column(nullable = false)
	private PetKindEnum kind;
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "owner_id", nullable = false)
	private PetOwner owner;
}
