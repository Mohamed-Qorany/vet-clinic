package com.vet.clinic.entity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vet.clinic.entity.base.EntityBaseUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Doctor extends EntityBaseUser implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String bio;
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "clinic_id", nullable = true)
	private Clinic clinic;
}
