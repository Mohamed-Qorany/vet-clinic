package com.vet.clinic.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vet.clinic.entity.base.EntityBaseId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Clinic extends EntityBaseId implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(nullable = false)
	private String name;
	@Column(unique = true, nullable = false)
	private String phone;
	@Column(nullable = false)
	private String address;
	@Column(unique = true, nullable = false)
	private String email;
	@Column(nullable = false)
	private String workingDays;
	@Column(nullable = false)
	private Date startTime;
	@Column(nullable = false)
	private Date endTime;
	@OneToMany(mappedBy = "clinic", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Doctor> doctors;
}
