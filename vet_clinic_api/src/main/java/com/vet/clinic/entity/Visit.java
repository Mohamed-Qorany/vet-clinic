package com.vet.clinic.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vet.clinic.entity.base.EntityBaseId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Visit extends EntityBaseId implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(nullable = false)
	private Date date;
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pet_id")
	private Pet pet;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "doctor_id")
	private Doctor doctor;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "clinic_id")
	private Clinic clinic;
}
