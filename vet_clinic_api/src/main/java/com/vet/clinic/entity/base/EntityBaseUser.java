package com.vet.clinic.entity.base;

import com.vet.clinic.enums.GenderEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public class EntityBaseUser extends EntityBaseId {

    @Column(nullable = false)
    private String name;
    @Column(unique = true, nullable = false)
    private String phone;
    @Column(nullable = false)
    private GenderEnum gender;
}
