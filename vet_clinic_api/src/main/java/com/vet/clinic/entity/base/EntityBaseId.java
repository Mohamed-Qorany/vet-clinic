package com.vet.clinic.entity.base;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public class EntityBaseId {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
}
