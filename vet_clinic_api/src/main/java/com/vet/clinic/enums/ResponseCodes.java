package com.vet.clinic.enums;

public enum ResponseCodes {
	ERROR (0, "ERROR"),
	SUCCESS (1, "SUCCESS"),
	NOT_FOUND (2, "RESOURCE NOT FOUND"),

    //	------------- for CLINICS ( 2 -> 20 ) -------------
	CLINIC_NOT_FOUND (2, "CLINIC NOT FOUND"),

	//	------------- for DOCTORS ( 20 -> 40 ) -------------
	DOCTOR_NOT_FOUND (21, "DOCTOR NOT FOUND"),

	//	------------- for DOCTORS ( 40 -> 60 ) -------------
	PET_NOT_FOUND (41, "PET NOT FOUND");


	private final Long code;
	private final String description;

	private ResponseCodes(long code, String description) {
		this.code = code;
		this.description = description;
	}
	public String getDescription() { return description; }
	public long getCode() { return code; }

	@Override
	public String toString() {
		return code.toString() + " : " + description;
	}
}
