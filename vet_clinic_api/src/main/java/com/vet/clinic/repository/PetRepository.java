package com.vet.clinic.repository;

import com.vet.clinic.entity.Pet;
import com.vet.clinic.entity.projection.PetProjection;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "pet", path = "pet",
        excerptProjection = PetProjection.class)
public interface PetRepository extends PagingAndSortingRepository<Pet, Long> { }