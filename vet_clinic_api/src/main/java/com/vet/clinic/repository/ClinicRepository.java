package com.vet.clinic.repository;

import com.vet.clinic.entity.Clinic;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "clinic", path = "clinic")
public interface ClinicRepository extends PagingAndSortingRepository<Clinic, Long> {

    @RestResource(path = "findClinic", rel = "findClinic")
    List<Clinic> findByPhoneContainingOrAddressContaining(String phone, String address);
}