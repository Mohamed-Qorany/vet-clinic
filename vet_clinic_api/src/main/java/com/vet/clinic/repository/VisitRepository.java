package com.vet.clinic.repository;

import com.vet.clinic.entity.Visit;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "visit", path = "visit")
public interface VisitRepository extends PagingAndSortingRepository<Visit, Long> {


}