package com.vet.clinic.repository;

import com.vet.clinic.entity.PetOwner;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "pet_owner", path = "pet_owner")
public interface PetOwnerRepository extends PagingAndSortingRepository<PetOwner, Long> { }