package com.vet.clinic.controller;

import com.vet.clinic.VetUtil;
import com.vet.clinic.entity.Clinic;
import com.vet.clinic.entity.Doctor;
import com.vet.clinic.enums.ResponseCodes;
import com.vet.clinic.model.DoctorModel;
import com.vet.clinic.repository.ClinicRepository;
import com.vet.clinic.repository.DoctorRepository;
import com.vet.clinic.service.AmazonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;


@CrossOrigin
@RepositoryRestController
public class DoctorController {

    @Autowired
    AmazonClient amazonClient;
    @Autowired
    DoctorRepository doctorRepository;
    @Autowired
    ClinicRepository clinicRepository;


    @RequestMapping(value = "/doctor", method = RequestMethod.POST)
    public ResponseEntity save(@RequestBody DoctorModel doctorModel){
        Optional<Clinic> clinicOptional = clinicRepository.findById(doctorModel.getClinicId());
        if( clinicOptional.isPresent() ){
            try{
                String bioPath = amazonClient.uploadFile(doctorModel.getBio());
                Doctor doctor = new Doctor(bioPath, clinicOptional.get());
                doctor.setName(doctorModel.getName());
                doctor.setPhone(doctorModel.getPhone());
                doctor.setGender(doctorModel.getGender());
                doctor = doctorRepository.save(doctor);
                return VetUtil.buildResponse(ResponseCodes.SUCCESS, doctor);
            }catch (Exception ex){
                ex.printStackTrace();
                return VetUtil.buildResponse(ResponseCodes.ERROR, null);
            }
        }  else {
            return VetUtil.buildResponse(ResponseCodes.CLINIC_NOT_FOUND, null);
        }
    }

    @RequestMapping(value = "/doctor/{id}", method = RequestMethod.PUT)
    public ResponseEntity  update(@RequestBody DoctorModel doctorModel, @PathVariable Long id){
        Optional<Doctor> doctorOptional = doctorRepository.findById(id);
        Optional<Clinic> clinicOptional = clinicRepository.findById(doctorModel.getClinicId());
        if( doctorOptional.isPresent() ){
            try{
                Doctor doctor = doctorOptional.get();
                if( doctorModel.getBio() != null ){
                    doctor.setBio(amazonClient.uploadFile(doctorModel.getBio()));
                }
                doctor.setName(doctorModel.getName());
                doctor.setPhone(doctorModel.getPhone());
                doctor.setGender(doctorModel.getGender());
                if( clinicOptional.isPresent() ){
                    doctor.setClinic(clinicOptional.get());
                } else {
                    return VetUtil.buildResponse(ResponseCodes.CLINIC_NOT_FOUND, doctor);
                }
                doctor = doctorRepository.save(doctor);
                return VetUtil.buildResponse(ResponseCodes.SUCCESS, doctor);
            }catch (Exception ex){
                ex.printStackTrace();
                return VetUtil.buildResponse(ResponseCodes.ERROR, null);
            }
        }  else {
            return VetUtil.buildResponse(ResponseCodes.DOCTOR_NOT_FOUND, null);
        }
    }

    @RequestMapping(value = "/doctor/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable Long id){
        Optional<Doctor> doctorOptional = doctorRepository.findById(id);
        if( doctorOptional.isPresent() ){
            try{
                Doctor doctor = doctorOptional.get();
                amazonClient.deleteFile(doctor.getBio());
                doctorRepository.delete(doctor);
                return VetUtil.buildResponse(ResponseCodes.SUCCESS, null);
            }catch (Exception ex){
                ex.printStackTrace();
                return VetUtil.buildResponse(ResponseCodes.CLINIC_NOT_FOUND, null);
            }
        } else {
            return VetUtil.buildResponse(ResponseCodes.DOCTOR_NOT_FOUND, null);
        }
    }

    @RequestMapping(value = "/doctor/{id}", method = RequestMethod.GET)
    public ResponseEntity findById(@PathVariable Long id){
        Optional<Doctor> doctorOptional = doctorRepository.findById(id);
        if( doctorOptional.isPresent() ){
            DoctorModel doctor = new DoctorModel();
            BeanUtils.copyProperties(doctorOptional.get(), doctor);
            doctor.setClinicId(doctorOptional.get().getClinic() != null ? doctorOptional.get().getClinic().getId(): 0);
            doctor.setBioPath(doctorOptional.get().getBio());
            return VetUtil.buildResponse(ResponseCodes.SUCCESS, doctor);
        } else {
            return VetUtil.buildResponse(ResponseCodes.DOCTOR_NOT_FOUND, null);
        }
    }
}
