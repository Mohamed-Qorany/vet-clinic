package com.vet.clinic.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VetResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	long code;
	String msg;
	Object data;

	public VetResponse(long code, String msg) {
		this.code = code;
		this.msg = msg;
	}
}
