package com.vet.clinic.model;

import com.vet.clinic.enums.GenderEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DoctorModel {
	private long id;
	private String name;
	private String phone;
	private GenderEnum gender;
	private FileModel bio;
	private String bioPath;
	private long clinicId;
	private String address;
}
