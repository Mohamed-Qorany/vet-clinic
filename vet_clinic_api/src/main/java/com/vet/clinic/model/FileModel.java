package com.vet.clinic.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FileModel implements Serializable{

	private Long fileId;
	private String filePath;
	private String fileName;
	private long fileSize;
	private String fileType;
	private byte[] content;
}
