package com.vet.clinic.config;

import com.vet.clinic.entity.*;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.stereotype.Component;

@Component
public class SpringDataRestCustomization extends RepositoryRestConfigurerAdapter {

 @Override
 public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
    config.getCorsRegistry()
    .addMapping("/**")
    .allowedOrigins("*")
    .allowedMethods("*")
    .maxAge(3600);
    config.exposeIdsFor(Pet.class);
    config.exposeIdsFor(PetOwner.class);
    config.exposeIdsFor(Clinic.class);
    config.exposeIdsFor(Doctor.class);
    config.exposeIdsFor(Visit.class);
  }
}