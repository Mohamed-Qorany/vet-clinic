# Vet Clinic APP


## UI (vet-clinic-ui)
Technologies : 
Angular latest Version 

Run :
1) go to path /vet-clinic-ui
2) run command -> run npm i
3) run command -> ng serve
then go to http://localhost:4200/






## API  (vet_clinic_api)
Technologies : 
Spring boot (Web, JPA and Rest Repositories )
Mysql
Hibernate
liquibase
lombok
Maven
AWS integration for hosting files and photos

Run :
1) go to path /vet_clinic_api
2) import project as spring boot maven projec
3) run the app
then go to http://localhost:2200/vet/
